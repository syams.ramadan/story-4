# Generated by Django 2.2.6 on 2019-10-08 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('judul', models.CharField(max_length=50)),
                ('lokasi', models.CharField(max_length=100)),
                ('tanggal_waktu', models.DateTimeField()),
                ('kategori', models.CharField(max_length=100)),
            ],
        ),
    ]

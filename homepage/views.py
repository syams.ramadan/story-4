from django.shortcuts import render,redirect
from .models import Jadwal
from . import forms

def Work(request):
    return render(request, 'home.html')
    
def About_Me(request):
    return render(request, 'story2-page-about-me.html')
    
def jadwal(request):
    schedules = Jadwal.objects.all().order_by('tanggal_waktu')
    return render(request, 'html-form.html',{'schedules':schedules})
    
def buat_jadwal(request):
    if request.method == 'POST':
        form = forms.BuatJadwal(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:Jadwal')
    else :
        form = forms.BuatJadwal()
    return render(request, 'form.html', {'form':form})
    
def hapus_jadwal(request):
    if request.method == "POST":
        id = request.POST['id']
        Jadwal.objects.get(id=id).delete()
    return redirect('homepage:Jadwal')


# Create your views here.

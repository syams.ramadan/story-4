from django.db import models

class Jadwal(models.Model):
    judul = models.CharField(max_length=50)
    lokasi = models.CharField(max_length=100)
    tanggal_waktu = models.DateTimeField()
    kategori = models.CharField(max_length=100)
    
    def __str__(self):
        return self.judul

# Create your models here.

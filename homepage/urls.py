from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.Work, name='Work'),
    path('about me/', views.About_Me, name='About_Me'),
    path('jadwal/', views.jadwal, name='Jadwal'),
    path('jadwal/create', views.buat_jadwal, name ="Buat_Jadwal"),
    path('jadwal/delete', views.hapus_jadwal, name ="Hapus_Jadwal")
    # dilanjutkan ...
]
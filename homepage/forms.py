from django import forms
from .models import Jadwal

class BuatJadwal(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ['judul','lokasi','tanggal_waktu','kategori']